#!/bin/bash

if [ $# -gt 0 ]; then
	case $1 in 
		10 | 100 | 1000)
			# echo "Reset speed..."
			sudo ethtool -s enp0s3 speed $1 duplex full autoneg on
			ethtool enp0s3 2> /dev/null | grep 'Advertised link modes' | sed "s/^ *[^ ]Advertised link modes:  //"
			# echo "Successfully changed the speed."
			;;
		*)
			echo "Invalid speed..."
			;;
	esac
else
	# echo "Usage: script <speed>"
	ethtool enp0s3 2> /dev/null | grep 'Advertised link modes' | sed "s/^ *[^ ]Advertised link modes:  //"
fi
