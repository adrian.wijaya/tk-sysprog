#!/bin/bash

echo 'export PATH=$PATH:~/.local/bin:/bin:/usr/bin:/sbin' >> /home/user/.bashrc
source /home/user/.bashrc

cp /home/user/sysprog-group/control_driver.sh /home/user/sysprog-group/control_driver
chmod +x /home/user/sysprog-group/control_driver
mv /home/user/sysprog-group/control_driver /home/user/.local/bin/

python3 webapp/manage.py runserver 10.0.2.15:8000
