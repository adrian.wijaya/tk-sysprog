from django import forms

pilihan = (
    (10, "10"),
    (100, "100"),
    (1000, "1000"),
)

class Form(forms.Form):
    speed = forms.ChoiceField(label='Ethernet speed', choices = pilihan)