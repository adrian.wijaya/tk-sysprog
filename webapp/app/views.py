from django.shortcuts import render
from django.http import HttpResponse
from .forms import Form
from django.contrib import messages
import os

# Create your views here.

def bootscript(request):
    context = {}
    context['form'] = Form()

    if request.GET: 
        speed = request.GET['speed']
    else:
        speed = ""

    ret = os.popen("/home/user/.local/bin/control_driver "+speed)
    out = ret.read().strip()
    context['speed'] = out

    return render(request, "base.html", context)
