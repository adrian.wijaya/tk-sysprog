# TK Sysprog
## Kill Dash Nine
- Adrian Wijaya (1806205363)
- Alvin Hariman (1806205432)
- Inigo Ramli   (1806133742)

## HOW TO
- Simply run the vm and (depending on the port forwarding config) the web apps can be run on host machine.

## Usage
- Control driver script has <speed> parameter that can change ethernet speed.
- Valid Speed: 10,100,1000 Mbps
